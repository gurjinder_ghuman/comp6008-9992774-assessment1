﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GameApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void ButtonClicked(object sender, EventArgs e)
        {
            string _input = UserInput.Text;
            Return.Text = Class1.Check(_input).ToString();

        }
    }
}
