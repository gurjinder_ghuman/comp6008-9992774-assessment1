﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GameApp1
{
    public partial class MainPage : ContentPage
    {
        public int rand;
        public int guesses;

        public MainPage()
        {
            InitializeComponent();
            Random gen = new Random();
            rand = gen.Next(1, 5);
        }


        void ButtonClicked(object sender, EventArgs e)
        {
            var _input = UserInput.Text;
            var input = Convert.ToInt32(_input);

            var answer = Class1.Check(input, rand);

            if (answer == 0)
            {

                guesses = guesses + 1;
                score.Text = $"Correct Guesses: {guesses}";
                BackLayout.BackgroundColor = Color.Green;
                playAgain.Text = "Guess again to play again";
                UserInput.Placeholder = "Input Number";
                Random gen = new Random();
                rand = gen.Next(1, 5);
            }
            if (answer != 0)
            {

                BackLayout.BackgroundColor = Color.Red;
            }

        }
    }
}




